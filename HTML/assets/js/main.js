jQuery(document).ready(function($){
	
        
    // Call Gridder
    $(".gridder").gridderExpander({
        scrollOffset: 140,
        scrollTo: "panel", // "panel" or "listitem"
        animationSpeed: 500,
        animationEasing: "easeInOutExpo",
        onStart: function () {
            console.log("Gridder Inititialized");
        },
        onExpanded: function (object) {
            console.log("Gridder Expanded");
            $(".carousel").carousel();
        },
        onChanged: function (object) {
            console.log("Gridder Changed");
        },
        onClosed: function () {
            console.log("Gridder Closed");
        }
    });
    
    
    // Lazy load
    $('.lazy').Lazy({
        // your configuration goes here 
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        effectTime: 900
    });
    
    
    /*!
    * https://github.com/iamdustan/smoothscroll
    */
    // scroll into view
    document.querySelector('.js-scroll').addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('#courses').scrollIntoView({ behavior: 'smooth' });
    });
    
    document.querySelector('.js-scroll-2').addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('#courses').scrollIntoView({ behavior: 'smooth' });
    });
    
});