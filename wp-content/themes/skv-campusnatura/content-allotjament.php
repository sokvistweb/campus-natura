<section id="camping" class="wrapper special staying">
    <?php if(function_exists('qtranxf_getLanguage')) { ?>
    <?php if (qtranxf_getLanguage()=='ca'): ?>
    
    <div class="inner">
        <header class="major">
            <h2>Necessites allotjament durant el curs?</h2>
            <p><span>Allotjament</span></p>
            <p>Com a alumne tens parcel·la gratis al càmping. També pots llogar un mobilhome amb descompte especial. Informa't a la recepció trucant al <a href="tel:0034972751630">972 75 16 30</a> o per e-mail al <a href="mailto:reserves@camping-castellmar.com" title="Envian's un email">reserves@camping-castellmar.com</a>.</p>
        </header>
        
        <ul class="campsite major">
            <li><span><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/camping-1.jpg" src="" alt="" width="260" height="260" /></span></li>
            <li><span><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/camping-2.jpg" src="" alt="" width="260" height="260" /></span></li>
            <li><span><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/camping-3.jpg" src="" alt="" width="260" height="260" /></span></li>
        </ul>

        <p><span>Dinars i sopars</span></p>
        <p>El càmping ofereix menús especials i pensió completa pels alumnes i acompanyants. Informa't!</p>

        <p><strong>Porta els acompanyants que vulguis!!</strong></p>
        <p>Mentre tu fas el curs, ells tindran accés a tots els serveis del càmping: banys, dutxes, piscina, animació...</p>


        <h2>Ens trobem aquí</h2>

        <p>Les formacions tindran lloc al <a href="https://www.camping-castellmar.com/" title="Web del Càmping Castell Mar">Càmping Castell Mar</a> i rodalies Som al terme de Castelló d'Empúries, al Parc Natural dels Aiguamolls de l'Empordà i a només 1500m de la zona integral. Al càmping treballem per a restablir i mantenir l'equilibri natural de l'ecosistema que ens acull. Els nostres biòlegs vetllen diàriament perquè l'entorn natural d'aquesta àrea pugui créixer i desenvolupar-se al seu ritme. Fauna i flora troben aquí un santuari de protecció.</p>

        <a class="google" href="https://goo.gl/maps/K12SuoGTnR52" title="Veure a Google Maps" target="_blank"><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/google-maps-camping-castell-mar.png" src="" alt="Càmping Castell Mar al Mapa de google" width="1142" height="400" /></a>

        <small><a class="google" href="https://goo.gl/maps/K12SuoGTnR52" title="Veure a Google Maps" target="_blank">Veure la localització a Google Maps</a></small>
    </div>
    
    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='es'): ?>
    
    <div class="inner">
        <header class="major">
            <h2>¿Necesitas alojamiento durante el curso?</h2>
            <p><span>Alojamiento</span></p>
            <p>Como alumno tienes parcela gratis al camping. También puedes alquilar un mobilhome con descuento especial. Infórmate en la recepción llamando al <a href="tel:0034972751630">972 75 16 30</a> o por e-mail a <a href="mailto:reserves@camping-castellmar.com" title="Envian's un email">reserves@camping-castellmar.com</a>.</p>
        </header>
        
        <ul class="campsite major">
            <li><span><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/camping-1.jpg" src="" alt="" width="260" height="260" /></span></li>
            <li><span><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/camping-2.jpg" src="" alt="" width="260" height="260" /></span></li>
            <li><span><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/camping-3.jpg" src="" alt="" width="260" height="260" /></span></li>
        </ul>

        <p><span>Comidas y cenas</span></p>
        <p>El camping ofrece menús especiales y pensión completa por los alumnos y acompañantes. ¡Infórmate!</p>

        <p><strong>Lleva los acompañantes que quieras!!</strong></p>
        <p>Mientras tú realizas el curso, ellos tendrán acceso a todos los servicios del camping: baños, duchas, piscina, animación...</p>


        <h2>Estamos aquí</h2>

        <p>Las formaciones tendrán lugar en el <a href="https://www.camping-castellmar.com/" title="Web del Càmping Castell Mar">Càmping Castell Mar</a> y alrededores. Estamos en el término de Castelló d'Empúries, en el Parque Natural de los Aiguamolls del Empordà y a sólo 1500m de la zona integral. En el camping trabajamos para restablecer y mantener el equilibrio natural del ecosistema que nos acoge. Nuestros biólogos velan diariamente para que el entorno natural de esta área pueda crecer y desarrollarse a su ritmo. Fauna y flora encuentran aquí un santuario de protección.</p>

        <a class="google" href="https://goo.gl/maps/K12SuoGTnR52" title="Veure a Google Maps" target="_blank"><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/google-maps-camping-castell-mar.png" src="" alt="Càmping Castell Mar al Mapa de google" width="1142" height="400" /></a>

        <small><a class="google" href="https://goo.gl/maps/K12SuoGTnR52" title="Veure a Google Maps" target="_blank">Ver la localización en Google Maps</a></small>
    </div>
    
    <?php endif; ?>
    <?php } ?>
</section>