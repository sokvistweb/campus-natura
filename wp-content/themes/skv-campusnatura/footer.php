
    <!-- Footer -->
    <footer id="footer">
        <?php if(function_exists('qtranxf_getLanguage')) { ?>
        <?php if (qtranxf_getLanguage()=='ca'): ?>
        <ul class="icons legal">
            <li><a href="https://www.camping-castellmar.com/avis-legal/" title="Llegeix la nostra política legal" target="_blank">Avís legal</a></li>
            <li><a href="https://www.camping-castellmar.com/politica-de-privacitat/" title="Llegeix la nostra política de privacitat" target="_blank">Política de privacitat</a></li>
        </ul>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='es'): ?>
        <ul class="icons legal">
            <li><a href="https://www.camping-castellmar.com/es/avis-legal/" title="Lee nuestra política legal" target="_blank">Aviso legal</a></li>
            <li><a href="https://www.camping-castellmar.com/es/politica-de-privacidad/" title="Lee nuestra política de privacidad" target="_blank">Política de privacidad</a></li>
        </ul>
        <?php endif; ?>
        <?php } ?>
        
        <ul class="copyright">
            <li>&copy; Camping Castell Mar</li><li>Design: <a href="https://sokvist.com">Sokvist, disseny i marketing</a></li>
        </ul>
    </footer>
    
    
    <?php wp_footer(); ?>
    
    <script type="text/javascript">
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"https:\/\/campusnatura.org\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
    /* ]]> */
    </script>
    
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/plugins.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/main.min.js"></script>


</body>
</html>
