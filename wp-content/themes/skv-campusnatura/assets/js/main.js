jQuery(document).ready(function($){
	
        
    // Call Gridder
    $(".gridder").gridderExpander({
        scrollOffset: 140,
        scrollTo: "panel", // "panel" or "listitem"
        animationSpeed: 500,
        animationEasing: "easeInOutExpo"
    });
    
    
    // Lazy load
    $('.lazy').Lazy({
        // your configuration goes here 
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        effectTime: 900
    });
    
    
    /*
    A simple jQuery modal (http://github.com/kylefox/jquery-modal)
    Version 0.9.2
    */
    //$(".modal").modal({});
    
    
    /*!
    * https://github.com/iamdustan/smoothscroll
    */
    // scroll into view
    document.querySelector('.js-scroll').addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('#courses').scrollIntoView({ behavior: 'smooth' });
    });
    
    document.querySelector('.js-scroll-2').addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('#courses').scrollIntoView({ behavior: 'smooth' });
    });
    
    
});


