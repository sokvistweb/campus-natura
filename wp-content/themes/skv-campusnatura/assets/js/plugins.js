// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


// Place any jQuery/helper plugins in here.


/*
 *  Gridder - v1.4.2
 *  A jQuery plugin that displays a thumbnail grid expanding preview similar to the effect seen on Google Images.
 *  http://www.oriongunning.com/
 *
 *  Made by Orion Gunning
 *  Under MIT License
 */
!function(a){window.console=window.console||function(){var a={};return a.log=a.warn=a.debug=a.info=a.error=a.time=a.dir=a.profile=a.clear=a.exception=a.trace=a.assert=function(){},a}(),a.fn.extend(a.easing,{def:"easeInOutExpo",easeInOutExpo:function(a,b,c,d,e){return 0===b?c:b===e?c+d:(b/=e/2)<1?d/2*Math.pow(2,10*(b-1))+c:d/2*(-Math.pow(2,-10*--b)+2)+c}}),a(document).keydown(function(b){var c=b.keyCode,d=a(".currentGridder"),e=d.find(".gridder-show");d.length&&(37===c&&(e.prev().prev().trigger("click"),b.preventDefault()),39===c&&(e.next().trigger("click"),b.preventDefault()))}),a.fn.gridderExpander=function(b){var c=a.extend({},a.fn.gridderExpander.defaults,b);return this.each(function(){function b(b){c.scroll&&a("html, body").animate({scrollTop:b.find(".selectedItem").offset().top-c.scrollOffset},{duration:200,easing:c.animationEasing}),g.removeClass("hasSelectedItem"),h=!1,b.find(".selectedItem").removeClass("selectedItem"),b.find(".gridder-show").slideUp(c.animationSpeed,c.animationEasing,function(){b.find(".gridder-show").remove(),c.onClosed(b)}),a(".currentGridder").removeClass("currentGridder")}function d(d){if(a(".currentGridder").removeClass("currentGridder"),g.addClass("currentGridder"),d.hasClass("selectedItem"))return void b(g,c);g.find(".selectedItem").removeClass("selectedItem"),d.addClass("selectedItem"),g.find(".gridder-show").remove(),g.hasClass("hasSelectedItem")||g.addClass("hasSelectedItem");var h=a('<div class="gridder-show loading"></div>');f=h.insertAfter(d);var i="";0===d.data("griddercontent").indexOf("#")?(i=a(d.data("griddercontent")).html(),e(d,i)):a.ajax({type:"GET",url:d.data("griddercontent"),success:function(a){i=a,e(d,i)},error:function(a){i=a.responseText,e(d,i)}})}function e(b,d){var e='<div class="gridder-padding">';if(c.showNav){var g=a(".selectedItem").prev(),i=a(".selectedItem").next().next();e+='<div class="gridder-navigation">',e+='<a href="#" class="gridder-close">'+c.closeText+"</a>",e+='<a href="#" class="gridder-nav prev '+(g.length?"":"disabled")+'">'+c.prevText+"</a>",e+='<a href="#" class="gridder-nav next '+(i.length?"":"disabled")+'">'+c.nextText+"</a>",e+="</div>"}if(e+='<div class="gridder-expanded-content">',e+=d,e+="</div>",e+="</div>",h?(f.html(e),f.find(".gridder-padding").fadeIn(c.animationSpeed,c.animationEasing,function(){h=!0,a.isFunction(c.onContent)&&c.onContent(f)})):f.hide().append(e).slideDown(c.animationSpeed,c.animationEasing,function(){h=!0,a.isFunction(c.onContent)&&c.onContent(f)}),c.scroll){var j="panel"===c.scrollTo?b.offset().top+b.height()-c.scrollOffset:b.offset().top-c.scrollOffset;a("html, body").animate({scrollTop:j},{duration:c.animationSpeed,easing:c.animationEasing})}f.removeClass("loading")}var f,g=a(this),h=!1;c.onStart(g),g.on("click",".gridder-list",function(b){b.preventDefault();var c=a(this);d(c)}),g.on("click",".gridder-nav.next",function(b){b.preventDefault(),a(this).parents(".gridder-show").next().trigger("click")}),g.on("click",".gridder-nav.prev",function(b){b.preventDefault(),a(this).parents(".gridder-show").prev().prev().trigger("click")}),g.on("click",".gridder-close",function(a){a.preventDefault(),b(g)})})},a.fn.gridderExpander.defaults={scroll:!0,scrollOffset:30,scrollTo:"panel",animationSpeed:400,animationEasing:"easeInOutExpo",showNav:!0,nextText:"Next",prevText:"Previous",closeText:"Close",onStart:function(){},onContent:function(){},onClosed:function(){}}}(jQuery);


/*! jQuery & Zepto Lazy v1.7.10 - http://jquery.eisbehr.de/lazy - MIT&GPL-2.0 license - Copyright 2012-2018 Daniel 'Eisbehr' Kern */
!function(t,e){"use strict";function r(r,a,i,u,l){function f(){L=t.devicePixelRatio>1,i=c(i),a.delay>=0&&setTimeout(function(){s(!0)},a.delay),(a.delay<0||a.combined)&&(u.e=v(a.throttle,function(t){"resize"===t.type&&(w=B=-1),s(t.all)}),u.a=function(t){t=c(t),i.push.apply(i,t)},u.g=function(){return i=n(i).filter(function(){return!n(this).data(a.loadedName)})},u.f=function(t){for(var e=0;e<t.length;e++){var r=i.filter(function(){return this===t[e]});r.length&&s(!1,r)}},s(),n(a.appendScroll).on("scroll."+l+" resize."+l,u.e))}function c(t){var i=a.defaultImage,o=a.placeholder,u=a.imageBase,l=a.srcsetAttribute,f=a.loaderAttribute,c=a._f||{};t=n(t).filter(function(){var t=n(this),r=m(this);return!t.data(a.handledName)&&(t.attr(a.attribute)||t.attr(l)||t.attr(f)||c[r]!==e)}).data("plugin_"+a.name,r);for(var s=0,d=t.length;s<d;s++){var A=n(t[s]),g=m(t[s]),h=A.attr(a.imageBaseAttribute)||u;g===N&&h&&A.attr(l)&&A.attr(l,b(A.attr(l),h)),c[g]===e||A.attr(f)||A.attr(f,c[g]),g===N&&i&&!A.attr(E)?A.attr(E,i):g===N||!o||A.css(O)&&"none"!==A.css(O)||A.css(O,"url('"+o+"')")}return t}function s(t,e){if(!i.length)return void(a.autoDestroy&&r.destroy());for(var o=e||i,u=!1,l=a.imageBase||"",f=a.srcsetAttribute,c=a.handledName,s=0;s<o.length;s++)if(t||e||A(o[s])){var g=n(o[s]),h=m(o[s]),b=g.attr(a.attribute),v=g.attr(a.imageBaseAttribute)||l,p=g.attr(a.loaderAttribute);g.data(c)||a.visibleOnly&&!g.is(":visible")||!((b||g.attr(f))&&(h===N&&(v+b!==g.attr(E)||g.attr(f)!==g.attr(F))||h!==N&&v+b!==g.css(O))||p)||(u=!0,g.data(c,!0),d(g,h,v,p))}u&&(i=n(i).filter(function(){return!n(this).data(c)}))}function d(t,e,r,i){++z;var o=function(){y("onError",t),p(),o=n.noop};y("beforeLoad",t);var u=a.attribute,l=a.srcsetAttribute,f=a.sizesAttribute,c=a.retinaAttribute,s=a.removeAttribute,d=a.loadedName,A=t.attr(c);if(i){var g=function(){s&&t.removeAttr(a.loaderAttribute),t.data(d,!0),y(T,t),setTimeout(p,1),g=n.noop};t.off(I).one(I,o).one(D,g),y(i,t,function(e){e?(t.off(D),g()):(t.off(I),o())})||t.trigger(I)}else{var h=n(new Image);h.one(I,o).one(D,function(){t.hide(),e===N?t.attr(C,h.attr(C)).attr(F,h.attr(F)).attr(E,h.attr(E)):t.css(O,"url('"+h.attr(E)+"')"),t[a.effect](a.effectTime),s&&(t.removeAttr(u+" "+l+" "+c+" "+a.imageBaseAttribute),f!==C&&t.removeAttr(f)),t.data(d,!0),y(T,t),h.remove(),p()});var m=(L&&A?A:t.attr(u))||"";h.attr(C,t.attr(f)).attr(F,t.attr(l)).attr(E,m?r+m:null),h.complete&&h.trigger(D)}}function A(t){var e=t.getBoundingClientRect(),r=a.scrollDirection,n=a.threshold,i=h()+n>e.top&&-n<e.bottom,o=g()+n>e.left&&-n<e.right;return"vertical"===r?i:"horizontal"===r?o:i&&o}function g(){return w>=0?w:w=n(t).width()}function h(){return B>=0?B:B=n(t).height()}function m(t){return t.tagName.toLowerCase()}function b(t,e){if(e){var r=t.split(",");t="";for(var a=0,n=r.length;a<n;a++)t+=e+r[a].trim()+(a!==n-1?",":"")}return t}function v(t,e){var n,i=0;return function(o,u){function l(){i=+new Date,e.call(r,o)}var f=+new Date-i;n&&clearTimeout(n),f>t||!a.enableThrottle||u?l():n=setTimeout(l,t-f)}}function p(){--z,i.length||z||y("onFinishedAll")}function y(t,e,n){return!!(t=a[t])&&(t.apply(r,[].slice.call(arguments,1)),!0)}var z=0,w=-1,B=-1,L=!1,T="afterLoad",D="load",I="error",N="img",E="src",F="srcset",C="sizes",O="background-image";"event"===a.bind||o?f():n(t).on(D+"."+l,f)}function a(a,o){var u=this,l=n.extend({},u.config,o),f={},c=l.name+"-"+ ++i;return u.config=function(t,r){return r===e?l[t]:(l[t]=r,u)},u.addItems=function(t){return f.a&&f.a("string"===n.type(t)?n(t):t),u},u.getItems=function(){return f.g?f.g():{}},u.update=function(t){return f.e&&f.e({},!t),u},u.force=function(t){return f.f&&f.f("string"===n.type(t)?n(t):t),u},u.loadAll=function(){return f.e&&f.e({all:!0},!0),u},u.destroy=function(){return n(l.appendScroll).off("."+c,f.e),n(t).off("."+c),f={},e},r(u,l,a,f,c),l.chainable?a:u}var n=t.jQuery||t.Zepto,i=0,o=!1;n.fn.Lazy=n.fn.lazy=function(t){return new a(this,t)},n.Lazy=n.lazy=function(t,r,i){if(n.isFunction(r)&&(i=r,r=[]),n.isFunction(i)){t=n.isArray(t)?t:[t],r=n.isArray(r)?r:[r];for(var o=a.prototype.config,u=o._f||(o._f={}),l=0,f=t.length;l<f;l++)(o[t[l]]===e||n.isFunction(o[t[l]]))&&(o[t[l]]=i);for(var c=0,s=r.length;c<s;c++)u[r[c]]=t[0]}},a.prototype.config={name:"lazy",chainable:!0,autoDestroy:!0,bind:"load",threshold:500,visibleOnly:!1,appendScroll:t,scrollDirection:"both",imageBase:null,defaultImage:"data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",placeholder:null,delay:-1,combined:!1,attribute:"data-src",srcsetAttribute:"data-srcset",sizesAttribute:"data-sizes",retinaAttribute:"data-retina",loaderAttribute:"data-loader",imageBaseAttribute:"data-imagebase",removeAttribute:!0,handledName:"handled",loadedName:"loaded",effect:"show",effectTime:0,enableThrottle:!0,throttle:250,beforeLoad:e,afterLoad:e,onError:e,onFinishedAll:e},n(t).on("load",function(){o=!0})}(window);


/* Contact form 7 */
!function(e){"use strict";"undefined"!=typeof wpcf7&&null!==wpcf7&&(wpcf7=e.extend({cached:0,inputs:[]},wpcf7),e(function(){wpcf7.supportHtml5=function(){var t={},a=document.createElement("input");t.placeholder="placeholder"in a;return e.each(["email","url","tel","number","range","date"],function(e,i){a.setAttribute("type",i),t[i]="text"!==a.type}),t}(),e("div.wpcf7 > form").each(function(){var t=e(this);wpcf7.initForm(t),wpcf7.cached&&wpcf7.refill(t)})}),wpcf7.getId=function(t){return parseInt(e('input[name="_wpcf7"]',t).val(),10)},wpcf7.initForm=function(t){var a=e(t);a.submit(function(t){wpcf7.supportHtml5.placeholder||e("[placeholder].placeheld",a).each(function(t,a){e(a).val("").removeClass("placeheld")}),"function"==typeof window.FormData&&(wpcf7.submit(a),t.preventDefault())}),e(".wpcf7-submit",a).after('<span class="ajax-loader"></span>'),wpcf7.toggleSubmit(a),a.on("click",".wpcf7-acceptance",function(){wpcf7.toggleSubmit(a)}),e(".wpcf7-exclusive-checkbox",a).on("click","input:checkbox",function(){var t=e(this).attr("name");a.find('input:checkbox[name="'+t+'"]').not(this).prop("checked",!1)}),e(".wpcf7-list-item.has-free-text",a).each(function(){var t=e(":input.wpcf7-free-text",this),a=e(this).closest(".wpcf7-form-control");e(":checkbox, :radio",this).is(":checked")?t.prop("disabled",!1):t.prop("disabled",!0),a.on("change",":checkbox, :radio",function(){e(".has-free-text",a).find(":checkbox, :radio").is(":checked")?t.prop("disabled",!1).focus():t.prop("disabled",!0)})}),wpcf7.supportHtml5.placeholder||e("[placeholder]",a).each(function(){e(this).val(e(this).attr("placeholder")),e(this).addClass("placeheld"),e(this).focus(function(){e(this).hasClass("placeheld")&&e(this).val("").removeClass("placeheld")}),e(this).blur(function(){""===e(this).val()&&(e(this).val(e(this).attr("placeholder")),e(this).addClass("placeheld"))})}),wpcf7.jqueryUi&&!wpcf7.supportHtml5.date&&a.find('input.wpcf7-date[type="date"]').each(function(){e(this).datepicker({dateFormat:"yy-mm-dd",minDate:new Date(e(this).attr("min")),maxDate:new Date(e(this).attr("max"))})}),wpcf7.jqueryUi&&!wpcf7.supportHtml5.number&&a.find('input.wpcf7-number[type="number"]').each(function(){e(this).spinner({min:e(this).attr("min"),max:e(this).attr("max"),step:e(this).attr("step")})}),e(".wpcf7-character-count",a).each(function(){var t=e(this),i=t.attr("data-target-name"),n=t.hasClass("down"),c=parseInt(t.attr("data-starting-value"),10),s=parseInt(t.attr("data-maximum-value"),10),p=parseInt(t.attr("data-minimum-value"),10),o=function(a){var i=e(a).val().length,o=n?c-i:i;t.attr("data-current-value",o),t.text(o),s&&s<i?t.addClass("too-long"):t.removeClass("too-long"),p&&i<p?t.addClass("too-short"):t.removeClass("too-short")};e(':input[name="'+i+'"]',a).each(function(){o(this),e(this).keyup(function(){o(this)})})}),a.on("change",".wpcf7-validates-as-url",function(){var t=e.trim(e(this).val());t&&!t.match(/^[a-z][a-z0-9.+-]*:/i)&&-1!==t.indexOf(".")&&(t="http://"+(t=t.replace(/^\/+/,""))),e(this).val(t)})},wpcf7.submit=function(t){if("function"==typeof window.FormData){var a=e(t);e(".ajax-loader",a).addClass("is-active"),wpcf7.clearResponse(a);var i=new FormData(a.get(0)),n={id:a.closest("div.wpcf7").attr("id"),status:"init",inputs:[],formData:i};e.each(a.serializeArray(),function(e,t){if("_wpcf7"==t.name)n.contactFormId=t.value;else if("_wpcf7_version"==t.name)n.pluginVersion=t.value;else if("_wpcf7_locale"==t.name)n.contactFormLocale=t.value;else if("_wpcf7_unit_tag"==t.name)n.unitTag=t.value;else if("_wpcf7_container_post"==t.name)n.containerPostId=t.value;else if(t.name.match(/^_wpcf7_\w+_free_text_/)){var a=t.name.replace(/^_wpcf7_\w+_free_text_/,"");n.inputs.push({name:a+"-free-text",value:t.value})}else t.name.match(/^_/)||n.inputs.push(t)}),wpcf7.triggerEvent(a.closest("div.wpcf7"),"beforesubmit",n);e.ajax({type:"POST",url:wpcf7.apiSettings.getRoute("/contact-forms/"+wpcf7.getId(a)+"/feedback"),data:i,dataType:"json",processData:!1,contentType:!1}).done(function(t,i,c){!function(t,a,i,c){n.id=e(t.into).attr("id"),n.status=t.status,n.apiResponse=t;var s=e(".wpcf7-response-output",c);switch(t.status){case"validation_failed":e.each(t.invalidFields,function(t,a){e(a.into,c).each(function(){wpcf7.notValidTip(this,a.message),e(".wpcf7-form-control",this).addClass("wpcf7-not-valid"),e("[aria-invalid]",this).attr("aria-invalid","true")})}),s.addClass("wpcf7-validation-errors"),c.addClass("invalid"),wpcf7.triggerEvent(t.into,"invalid",n);break;case"acceptance_missing":s.addClass("wpcf7-acceptance-missing"),c.addClass("unaccepted"),wpcf7.triggerEvent(t.into,"unaccepted",n);break;case"spam":s.addClass("wpcf7-spam-blocked"),c.addClass("spam"),wpcf7.triggerEvent(t.into,"spam",n);break;case"aborted":s.addClass("wpcf7-aborted"),c.addClass("aborted"),wpcf7.triggerEvent(t.into,"aborted",n);break;case"mail_sent":s.addClass("wpcf7-mail-sent-ok"),c.addClass("sent"),wpcf7.triggerEvent(t.into,"mailsent",n);break;case"mail_failed":s.addClass("wpcf7-mail-sent-ng"),c.addClass("failed"),wpcf7.triggerEvent(t.into,"mailfailed",n);break;default:var p="custom-"+t.status.replace(/[^0-9a-z]+/i,"-");s.addClass("wpcf7-"+p),c.addClass(p)}wpcf7.refill(c,t),wpcf7.triggerEvent(t.into,"submit",n),"mail_sent"==t.status&&(c.each(function(){this.reset()}),wpcf7.toggleSubmit(c)),wpcf7.supportHtml5.placeholder||c.find("[placeholder].placeheld").each(function(t,a){e(a).val(e(a).attr("placeholder"))}),s.html("").append(t.message).slideDown("fast"),s.attr("role","alert"),e(".screen-reader-response",c.closest(".wpcf7")).each(function(){var a=e(this);if(a.html("").attr("role","").append(t.message),t.invalidFields){var i=e("<ul></ul>");e.each(t.invalidFields,function(t,a){if(a.idref)var n=e("<li></li>").append(e("<a></a>").attr("href","#"+a.idref).append(a.message));else n=e("<li></li>").append(a.message);i.append(n)}),a.append(i)}a.attr("role","alert").focus()})}(t,0,0,a),e(".ajax-loader",a).removeClass("is-active")}).fail(function(t,i,n){var c=e('<div class="ajax-error"></div>').text(n.message);a.after(c)})}},wpcf7.triggerEvent=function(t,a,i){var n=e(t),c=new CustomEvent("wpcf7"+a,{bubbles:!0,detail:i});n.get(0).dispatchEvent(c),n.trigger("wpcf7:"+a,i),n.trigger(a+".wpcf7",i)},wpcf7.toggleSubmit=function(t,a){var i=e(t),n=e("input:submit",i);void 0===a?i.hasClass("wpcf7-acceptance-as-validation")||(n.prop("disabled",!1),e(".wpcf7-acceptance",i).each(function(){var t=e(this),a=e("input:checkbox",t);if(!t.hasClass("optional")&&(t.hasClass("invert")&&a.is(":checked")||!t.hasClass("invert")&&!a.is(":checked")))return n.prop("disabled",!0),!1})):n.prop("disabled",!a)},wpcf7.notValidTip=function(t,a){var i=e(t);if(e(".wpcf7-not-valid-tip",i).remove(),e('<span role="alert" class="wpcf7-not-valid-tip"></span>').text(a).appendTo(i),i.is(".use-floating-validation-tip *")){var n=function(t){e(t).not(":hidden").animate({opacity:0},"fast",function(){e(this).css({"z-index":-100})})};i.on("mouseover",".wpcf7-not-valid-tip",function(){n(this)}),i.on("focus",":input",function(){n(e(".wpcf7-not-valid-tip",i))})}},wpcf7.refill=function(t,a){var i=e(t),n=function(t,a){e.each(a,function(e,a){t.find(':input[name="'+e+'"]').val(""),t.find("img.wpcf7-captcha-"+e).attr("src",a);var i=/([0-9]+)\.(png|gif|jpeg)$/.exec(a);t.find('input:hidden[name="_wpcf7_captcha_challenge_'+e+'"]').attr("value",i[1])})},c=function(t,a){e.each(a,function(e,a){t.find(':input[name="'+e+'"]').val(""),t.find(':input[name="'+e+'"]').siblings("span.wpcf7-quiz-label").text(a[0]),t.find('input:hidden[name="_wpcf7_quiz_answer_'+e+'"]').attr("value",a[1])})};void 0===a?e.ajax({type:"GET",url:wpcf7.apiSettings.getRoute("/contact-forms/"+wpcf7.getId(i)+"/refill"),beforeSend:function(e){var t=i.find(':input[name="_wpnonce"]').val();t&&e.setRequestHeader("X-WP-Nonce",t)},dataType:"json"}).done(function(e,t,a){e.captcha&&n(i,e.captcha),e.quiz&&c(i,e.quiz)}):(a.captcha&&n(i,a.captcha),a.quiz&&c(i,a.quiz))},wpcf7.clearResponse=function(t){var a=e(t);a.removeClass("invalid spam sent failed"),a.siblings(".screen-reader-response").html("").attr("role",""),e(".wpcf7-not-valid-tip",a).remove(),e("[aria-invalid]",a).attr("aria-invalid","false"),e(".wpcf7-form-control",a).removeClass("wpcf7-not-valid"),e(".wpcf7-response-output",a).hide().empty().removeAttr("role").removeClass("wpcf7-mail-sent-ok wpcf7-mail-sent-ng wpcf7-validation-errors wpcf7-spam-blocked")},wpcf7.apiSettings.getRoute=function(e){var t=wpcf7.apiSettings.root;return t=t.replace(wpcf7.apiSettings.namespace,wpcf7.apiSettings.namespace+e)})}(jQuery),function(){if("function"==typeof window.CustomEvent)return!1;function e(e,t){t=t||{bubbles:!1,cancelable:!1,detail:void 0};var a=document.createEvent("CustomEvent");return a.initCustomEvent(e,t.bubbles,t.cancelable,t.detail),a}e.prototype=window.Event.prototype,window.CustomEvent=e}();


/*
    A simple jQuery modal (http://github.com/kylefox/jquery-modal)
    Version 0.9.2
*/
!function(o){"object"==typeof module&&"object"==typeof module.exports?o(require("jquery"),window,document):o(jQuery,window,document)}(function(o,t,i,e){var s=[],l=function(){return s.length?s[s.length-1]:null},n=function(){var o,t=!1;for(o=s.length-1;o>=0;o--)s[o].$blocker&&(s[o].$blocker.toggleClass("current",!t).toggleClass("behind",t),t=!0)};o.modal=function(t,i){var e,n;if(this.$body=o("body"),this.options=o.extend({},o.modal.defaults,i),this.options.doFade=!isNaN(parseInt(this.options.fadeDuration,10)),this.$blocker=null,this.options.closeExisting)for(;o.modal.isActive();)o.modal.close();if(s.push(this),t.is("a"))if(n=t.attr("href"),this.anchor=t,/^#/.test(n)){if(this.$elm=o(n),1!==this.$elm.length)return null;this.$body.append(this.$elm),this.open()}else this.$elm=o("<div>"),this.$body.append(this.$elm),e=function(o,t){t.elm.remove()},this.showSpinner(),t.trigger(o.modal.AJAX_SEND),o.get(n).done(function(i){if(o.modal.isActive()){t.trigger(o.modal.AJAX_SUCCESS);var s=l();s.$elm.empty().append(i).on(o.modal.CLOSE,e),s.hideSpinner(),s.open(),t.trigger(o.modal.AJAX_COMPLETE)}}).fail(function(){t.trigger(o.modal.AJAX_FAIL);var i=l();i.hideSpinner(),s.pop(),t.trigger(o.modal.AJAX_COMPLETE)});else this.$elm=t,this.anchor=t,this.$body.append(this.$elm),this.open()},o.modal.prototype={constructor:o.modal,open:function(){var t=this;this.block(),this.anchor.blur(),this.options.doFade?setTimeout(function(){t.show()},this.options.fadeDuration*this.options.fadeDelay):this.show(),o(i).off("keydown.modal").on("keydown.modal",function(o){var t=l();27===o.which&&t.options.escapeClose&&t.close()}),this.options.clickClose&&this.$blocker.click(function(t){t.target===this&&o.modal.close()})},close:function(){s.pop(),this.unblock(),this.hide(),o.modal.isActive()||o(i).off("keydown.modal")},block:function(){this.$elm.trigger(o.modal.BEFORE_BLOCK,[this._ctx()]),this.$body.css("overflow","hidden"),this.$blocker=o('<div class="'+this.options.blockerClass+' blocker current"></div>').appendTo(this.$body),n(),this.options.doFade&&this.$blocker.css("opacity",0).animate({opacity:1},this.options.fadeDuration),this.$elm.trigger(o.modal.BLOCK,[this._ctx()])},unblock:function(t){!t&&this.options.doFade?this.$blocker.fadeOut(this.options.fadeDuration,this.unblock.bind(this,!0)):(this.$blocker.children().appendTo(this.$body),this.$blocker.remove(),this.$blocker=null,n(),o.modal.isActive()||this.$body.css("overflow",""))},show:function(){this.$elm.trigger(o.modal.BEFORE_OPEN,[this._ctx()]),this.options.showClose&&(this.closeButton=o('<a href="#close-modal" rel="modal:close" class="close-modal '+this.options.closeClass+'">'+this.options.closeText+"</a>"),this.$elm.append(this.closeButton)),this.$elm.addClass(this.options.modalClass).appendTo(this.$blocker),this.options.doFade?this.$elm.css({opacity:0,display:"inline-block"}).animate({opacity:1},this.options.fadeDuration):this.$elm.css("display","inline-block"),this.$elm.trigger(o.modal.OPEN,[this._ctx()])},hide:function(){this.$elm.trigger(o.modal.BEFORE_CLOSE,[this._ctx()]),this.closeButton&&this.closeButton.remove();var t=this;this.options.doFade?this.$elm.fadeOut(this.options.fadeDuration,function(){t.$elm.trigger(o.modal.AFTER_CLOSE,[t._ctx()])}):this.$elm.hide(0,function(){t.$elm.trigger(o.modal.AFTER_CLOSE,[t._ctx()])}),this.$elm.trigger(o.modal.CLOSE,[this._ctx()])},showSpinner:function(){this.options.showSpinner&&(this.spinner=this.spinner||o('<div class="'+this.options.modalClass+'-spinner"></div>').append(this.options.spinnerHtml),this.$body.append(this.spinner),this.spinner.show())},hideSpinner:function(){this.spinner&&this.spinner.remove()},_ctx:function(){return{elm:this.$elm,$elm:this.$elm,$blocker:this.$blocker,options:this.options,$anchor:this.anchor}}},o.modal.close=function(t){if(o.modal.isActive()){t&&t.preventDefault();var i=l();return i.close(),i.$elm}},o.modal.isActive=function(){return s.length>0},o.modal.getCurrent=l,o.modal.defaults={closeExisting:!0,escapeClose:!0,clickClose:!0,closeText:"Close",closeClass:"",modalClass:"modal",blockerClass:"jquery-modal",spinnerHtml:'<div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div>',showSpinner:!0,showClose:!0,fadeDuration:null,fadeDelay:1},o.modal.BEFORE_BLOCK="modal:before-block",o.modal.BLOCK="modal:block",o.modal.BEFORE_OPEN="modal:before-open",o.modal.OPEN="modal:open",o.modal.BEFORE_CLOSE="modal:before-close",o.modal.CLOSE="modal:close",o.modal.AFTER_CLOSE="modal:after-close",o.modal.AJAX_SEND="modal:ajax:send",o.modal.AJAX_SUCCESS="modal:ajax:success",o.modal.AJAX_FAIL="modal:ajax:fail",o.modal.AJAX_COMPLETE="modal:ajax:complete",o.fn.modal=function(t){return 1===this.length&&new o.modal(this,t),this},o(i).on("click.modal",'a[rel~="modal:close"]',o.modal.close),o(i).on("click.modal",'a[rel~="modal:open"]',function(t){t.preventDefault(),o(this).modal()})});


/*!
* https://github.com/iamdustan/smoothscroll
*/
'use strict';

// polyfill
function polyfill() {
  // aliases
  var w = window;
  var d = document;

  // return if scroll behavior is supported and polyfill is not forced
  if (
    'scrollBehavior' in d.documentElement.style &&
    w.__forceSmoothScrollPolyfill__ !== true
  ) {
    return;
  }

  // globals
  var Element = w.HTMLElement || w.Element;
  var SCROLL_TIME = 468;

  // object gathering original scroll methods
  var original = {
    scroll: w.scroll || w.scrollTo,
    scrollBy: w.scrollBy,
    elementScroll: Element.prototype.scroll || scrollElement,
    scrollIntoView: Element.prototype.scrollIntoView
  };

  // define timing method
  var now =
    w.performance && w.performance.now
      ? w.performance.now.bind(w.performance)
      : Date.now;

  /**
   * indicates if a the current browser is made by Microsoft
   * @method isMicrosoftBrowser
   * @param {String} userAgent
   * @returns {Boolean}
   */
  function isMicrosoftBrowser(userAgent) {
    var userAgentPatterns = ['MSIE ', 'Trident/', 'Edge/'];

    return new RegExp(userAgentPatterns.join('|')).test(userAgent);
  }

  /*
   * IE has rounding bug rounding down clientHeight and clientWidth and
   * rounding up scrollHeight and scrollWidth causing false positives
   * on hasScrollableSpace
   */
  var ROUNDING_TOLERANCE = isMicrosoftBrowser(w.navigator.userAgent) ? 1 : 0;

  /**
   * changes scroll position inside an element
   * @method scrollElement
   * @param {Number} x
   * @param {Number} y
   * @returns {undefined}
   */
  function scrollElement(x, y) {
    this.scrollLeft = x;
    this.scrollTop = y;
  }

  /**
   * returns result of applying ease math function to a number
   * @method ease
   * @param {Number} k
   * @returns {Number}
   */
  function ease(k) {
    return 0.5 * (1 - Math.cos(Math.PI * k));
  }

  /**
   * indicates if a smooth behavior should be applied
   * @method shouldBailOut
   * @param {Number|Object} firstArg
   * @returns {Boolean}
   */
  function shouldBailOut(firstArg) {
    if (
      firstArg === null ||
      typeof firstArg !== 'object' ||
      firstArg.behavior === undefined ||
      firstArg.behavior === 'auto' ||
      firstArg.behavior === 'instant'
    ) {
      // first argument is not an object/null
      // or behavior is auto, instant or undefined
      return true;
    }

    if (typeof firstArg === 'object' && firstArg.behavior === 'smooth') {
      // first argument is an object and behavior is smooth
      return false;
    }

    // throw error when behavior is not supported
    throw new TypeError(
      'behavior member of ScrollOptions ' +
        firstArg.behavior +
        ' is not a valid value for enumeration ScrollBehavior.'
    );
  }

  /**
   * indicates if an element has scrollable space in the provided axis
   * @method hasScrollableSpace
   * @param {Node} el
   * @param {String} axis
   * @returns {Boolean}
   */
  function hasScrollableSpace(el, axis) {
    if (axis === 'Y') {
      return el.clientHeight + ROUNDING_TOLERANCE < el.scrollHeight;
    }

    if (axis === 'X') {
      return el.clientWidth + ROUNDING_TOLERANCE < el.scrollWidth;
    }
  }

  /**
   * indicates if an element has a scrollable overflow property in the axis
   * @method canOverflow
   * @param {Node} el
   * @param {String} axis
   * @returns {Boolean}
   */
  function canOverflow(el, axis) {
    var overflowValue = w.getComputedStyle(el, null)['overflow' + axis];

    return overflowValue === 'auto' || overflowValue === 'scroll';
  }

  /**
   * indicates if an element can be scrolled in either axis
   * @method isScrollable
   * @param {Node} el
   * @param {String} axis
   * @returns {Boolean}
   */
  function isScrollable(el) {
    var isScrollableY = hasScrollableSpace(el, 'Y') && canOverflow(el, 'Y');
    var isScrollableX = hasScrollableSpace(el, 'X') && canOverflow(el, 'X');

    return isScrollableY || isScrollableX;
  }

  /**
   * finds scrollable parent of an element
   * @method findScrollableParent
   * @param {Node} el
   * @returns {Node} el
   */
  function findScrollableParent(el) {
    while (el !== d.body && isScrollable(el) === false) {
      el = el.parentNode;
    }

    return el;
  }

  /**
   * self invoked function that, given a context, steps through scrolling
   * @method step
   * @param {Object} context
   * @returns {undefined}
   */
  function step(context) {
    var time = now();
    var value;
    var currentX;
    var currentY;
    var elapsed = (time - context.startTime) / SCROLL_TIME;

    // avoid elapsed times higher than one
    elapsed = elapsed > 1 ? 1 : elapsed;

    // apply easing to elapsed time
    value = ease(elapsed);

    currentX = context.startX + (context.x - context.startX) * value;
    currentY = context.startY + (context.y - context.startY) * value;

    context.method.call(context.scrollable, currentX, currentY);

    // scroll more if we have not reached our destination
    if (currentX !== context.x || currentY !== context.y) {
      w.requestAnimationFrame(step.bind(w, context));
    }
  }

  /**
   * scrolls window or element with a smooth behavior
   * @method smoothScroll
   * @param {Object|Node} el
   * @param {Number} x
   * @param {Number} y
   * @returns {undefined}
   */
  function smoothScroll(el, x, y) {
    var scrollable;
    var startX;
    var startY;
    var method;
    var startTime = now();

    // define scroll context
    if (el === d.body) {
      scrollable = w;
      startX = w.scrollX || w.pageXOffset;
      startY = w.scrollY || w.pageYOffset;
      method = original.scroll;
    } else {
      scrollable = el;
      startX = el.scrollLeft;
      startY = el.scrollTop;
      method = scrollElement;
    }

    // scroll looping over a frame
    step({
      scrollable: scrollable,
      method: method,
      startTime: startTime,
      startX: startX,
      startY: startY,
      x: x,
      y: y
    });
  }

  // ORIGINAL METHODS OVERRIDES
  // w.scroll and w.scrollTo
  w.scroll = w.scrollTo = function() {
    // avoid action when no arguments are passed
    if (arguments[0] === undefined) {
      return;
    }

    // avoid smooth behavior if not required
    if (shouldBailOut(arguments[0]) === true) {
      original.scroll.call(
        w,
        arguments[0].left !== undefined
          ? arguments[0].left
          : typeof arguments[0] !== 'object'
            ? arguments[0]
            : w.scrollX || w.pageXOffset,
        // use top prop, second argument if present or fallback to scrollY
        arguments[0].top !== undefined
          ? arguments[0].top
          : arguments[1] !== undefined
            ? arguments[1]
            : w.scrollY || w.pageYOffset
      );

      return;
    }

    // LET THE SMOOTHNESS BEGIN!
    smoothScroll.call(
      w,
      d.body,
      arguments[0].left !== undefined
        ? ~~arguments[0].left
        : w.scrollX || w.pageXOffset,
      arguments[0].top !== undefined
        ? ~~arguments[0].top
        : w.scrollY || w.pageYOffset
    );
  };

  // w.scrollBy
  w.scrollBy = function() {
    // avoid action when no arguments are passed
    if (arguments[0] === undefined) {
      return;
    }

    // avoid smooth behavior if not required
    if (shouldBailOut(arguments[0])) {
      original.scrollBy.call(
        w,
        arguments[0].left !== undefined
          ? arguments[0].left
          : typeof arguments[0] !== 'object' ? arguments[0] : 0,
        arguments[0].top !== undefined
          ? arguments[0].top
          : arguments[1] !== undefined ? arguments[1] : 0
      );

      return;
    }

    // LET THE SMOOTHNESS BEGIN!
    smoothScroll.call(
      w,
      d.body,
      ~~arguments[0].left + (w.scrollX || w.pageXOffset),
      ~~arguments[0].top + (w.scrollY || w.pageYOffset)
    );
  };

  // Element.prototype.scroll and Element.prototype.scrollTo
  Element.prototype.scroll = Element.prototype.scrollTo = function() {
    // avoid action when no arguments are passed
    if (arguments[0] === undefined) {
      return;
    }

    // avoid smooth behavior if not required
    if (shouldBailOut(arguments[0]) === true) {
      // if one number is passed, throw error to match Firefox implementation
      if (typeof arguments[0] === 'number' && arguments[1] === undefined) {
        throw new SyntaxError('Value could not be converted');
      }

      original.elementScroll.call(
        this,
        // use left prop, first number argument or fallback to scrollLeft
        arguments[0].left !== undefined
          ? ~~arguments[0].left
          : typeof arguments[0] !== 'object' ? ~~arguments[0] : this.scrollLeft,
        // use top prop, second argument or fallback to scrollTop
        arguments[0].top !== undefined
          ? ~~arguments[0].top
          : arguments[1] !== undefined ? ~~arguments[1] : this.scrollTop
      );

      return;
    }

    var left = arguments[0].left;
    var top = arguments[0].top;

    // LET THE SMOOTHNESS BEGIN!
    smoothScroll.call(
      this,
      this,
      typeof left === 'undefined' ? this.scrollLeft : ~~left,
      typeof top === 'undefined' ? this.scrollTop : ~~top
    );
  };

  // Element.prototype.scrollBy
  Element.prototype.scrollBy = function() {
    // avoid action when no arguments are passed
    if (arguments[0] === undefined) {
      return;
    }

    // avoid smooth behavior if not required
    if (shouldBailOut(arguments[0]) === true) {
      original.elementScroll.call(
        this,
        arguments[0].left !== undefined
          ? ~~arguments[0].left + this.scrollLeft
          : ~~arguments[0] + this.scrollLeft,
        arguments[0].top !== undefined
          ? ~~arguments[0].top + this.scrollTop
          : ~~arguments[1] + this.scrollTop
      );

      return;
    }

    this.scroll({
      left: ~~arguments[0].left + this.scrollLeft,
      top: ~~arguments[0].top + this.scrollTop,
      behavior: arguments[0].behavior
    });
  };

  // Element.prototype.scrollIntoView
  Element.prototype.scrollIntoView = function() {
    // avoid smooth behavior if not required
    if (shouldBailOut(arguments[0]) === true) {
      original.scrollIntoView.call(
        this,
        arguments[0] === undefined ? true : arguments[0]
      );

      return;
    }

    // LET THE SMOOTHNESS BEGIN!
    var scrollableParent = findScrollableParent(this);
    var parentRects = scrollableParent.getBoundingClientRect();
    var clientRects = this.getBoundingClientRect();

    if (scrollableParent !== d.body) {
      // reveal element inside parent
      smoothScroll.call(
        this,
        scrollableParent,
        scrollableParent.scrollLeft + clientRects.left - parentRects.left,
        scrollableParent.scrollTop + clientRects.top - parentRects.top
      );

      // reveal parent in viewport unless is fixed
      if (w.getComputedStyle(scrollableParent).position !== 'fixed') {
        w.scrollBy({
          left: parentRects.left,
          top: parentRects.top,
          behavior: 'smooth'
        });
      }
    } else {
      // reveal element in viewport
      w.scrollBy({
        left: clientRects.left,
        top: clientRects.top,
        behavior: 'smooth'
      });
    }
  };
}

if (typeof exports === 'object' && typeof module !== 'undefined') {
  // commonjs
  module.exports = { polyfill: polyfill };
} else {
  // global
  polyfill();
}

