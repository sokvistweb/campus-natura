<!-- CTA -->
<section id="cta" class="wrapper">
    <?php if(function_exists('qtranxf_getLanguage')) { ?>
    <?php if (qtranxf_getLanguage()=='ca'): ?>
    
    <div class="inner">
        <header class="image">
            <h2>Subscriu-te al nostre newsletter</h2>
            <p>Estigues a prop de la natura, des del teu e-mail.</p>
        </header>
        <div class="actions">
            <form action="https://grupmascort.us20.list-manage.com/subscribe/post?u=a5c432e55add7a716b97f2956&amp;id=320f84772d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div class="footer-form newsletter-form field field-grouped is-revealing">
                   <p class="control control-expanded">
                        <input type="text" value="" name="NAME" class="input name" id="mce-NAME" placeholder="El teu nom" required>
                    </p>
                    <p class="control control-expanded">
                        <input type="email" value="" name="EMAIL" class="input email" id="mce-EMAIL" placeholder="El teu email" required>
                    </p>

                    <p class="control">
                        <input type="submit" value="Subscriu-te" name="subscribe" id="mc-embedded-subscribe" class="button button primary button-block button-shadow">
                    </p>
                </div>
            </form>
            <small>Tenim cura de les teves dades, tal com s’explica en la nostra <a href="https://www.camping-castellmar.com/politica-de-privacitat/" title="Llegeix la nostra política de privacitat" target="_blank">Política de privacitat</a>.</small>
        </div>
    </div>
    
    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='es'): ?>
    
    <div class="inner">
        <header class="image">
            <h2>Suscríbete a nuestro boletín</h2>
            <p>Mantente cerca de la naturaleza, desde tu e-mail.</p>
        </header>
        <div class="actions">
            <form action="https://grupmascort.us20.list-manage.com/subscribe/post?u=a5c432e55add7a716b97f2956&amp;id=320f84772d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div class="footer-form newsletter-form field field-grouped is-revealing">
                   <p class="control control-expanded">
                        <input type="text" value="" name="NAME" class="input name" id="mce-NAME" placeholder="Tu nombre" required>
                    </p>
                    <p class="control control-expanded">
                        <input type="email" value="" name="EMAIL" class="input email" id="mce-EMAIL" placeholder="Tu e-mail" required>
                    </p>

                    <p class="control">
                        <input type="submit" value="Suscríbete" name="subscribe" id="mc-embedded-subscribe" class="button button primary button-block button-shadow">
                    </p>
                </div>
            </form>
            <small>Cuidamos de tus datos, tal como se explica en nuestra <a href="https://www.camping-castellmar.com/es/politica-de-privacidad/" title="Llegeix la nostra política de privacitat" target="_blank">Política de privacidad</a>.</small>
        </div>
    </div>
    
    <?php endif; ?>
    <?php } ?>
</section>
