<?php get_header(); ?>



<section id="" class="wrapper special">
    <div class="inner">

    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <div class="spotlight">
            <div class="image">
                <!-- https://www.aliciaramirez.com/2014/03/advanced-custom-fields-image-object-tutorial/ -->
                <?php
                $imageArray = get_field('imatge_curs');
                $imageAlt = esc_attr($imageArray['alt']);
                $imageThumbURL = esc_url($imageArray['sizes']['large']);
                ?>
                <img class="lazy" data-src="<?php echo $imageThumbURL;?>" src="" alt="<?php echo $imageAlt; ?>">
            </div>
                
            <div class="content">
                <h2><?php the_title(); ?></h2>

                <?php the_content(); ?>
                
                <?php if( get_field('titol_formulari') || get_field('formulari') ): ?>
                <h3><?php the_field('titol_formulari'); ?></h3>
                <div role="form" class="wpcf7" lang="ca-ES" dir="ltr">
                    <?php the_field('formulari'); ?>
                </div>
                <?php endif; ?>
                
                <p class="whats">O contactan's per Whatsapp al <a href="tel:0034620273955">
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"><path d="M16.254 2C8.663 2 2.509 8.106 2.509 15.639c0 2.577.721 4.987 1.972 7.043L2 30l7.61-2.418a13.777 13.777 0 0 0 6.644 1.697C23.846 29.279 30 23.172 30 15.64S23.846 2.001 16.254 2.001zm6.835 18.819c-.323.802-1.786 1.533-2.431 1.567s-.663.5-4.178-1.027c-3.514-1.527-5.628-5.24-5.795-5.479s-1.361-1.94-1.297-3.653c.065-1.713 1.003-2.522 1.338-2.858s.718-.397.952-.401c.277-.005.456-.008.661-.001s.512-.043.778.665c.266.708.903 2.447.984 2.624s.132.383.006.611c-.126.228-.19.371-.373.568s-.386.44-.55.591c-.182.167-.373.348-.181.704s.853 1.522 1.861 2.484c1.295 1.236 2.412 1.646 2.756 1.832.345.188.55.167.764-.058.213-.225.915-.984 1.162-1.323s.479-.273.796-.146c.317.128 2.01 1.035 2.355 1.222s.575.283.657.431c.082.149.056.846-.267 1.647z"/></svg>
                620 27 39 55</a>.</p>
                
            </div>
        </div>
        
    <?php endwhile; ?>
    <?php endif; ?>

    </div>
</section>

<ul class="actions special">
    <li><a href="/" class="button primary">Veure tots el cursos</a></li>
</ul>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
