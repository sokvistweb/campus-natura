<!DOCTYPE html>
<html lang="ca">

<head>
    <meta charset="utf-8">
    <title>Campus Natura · Formació naturalista</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="description" content="Educació ambiental i naturalisme per a tothom, al camping Castell Mar, ubicat al Parc Natural dels Aiguamolls de l'Empordà.">
    <meta name="keywords" content="Naturalisme, medi ambient, Empordà, conservació, aiguamolls" />
    <meta name="author" content="Sokvist" />

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png">
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Averia+Serif+Libre:700|Source+Sans+Pro:400,600" rel="stylesheet">
    
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138582647-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-138582647-1');
    </script>
    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '1700102400282453'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=1700102400282453&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    
    
    <!-- Facebook Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://campusnatura.org/">
    <meta property="og:title" content="Campus Natura · Formació naturalista">
    <meta property="og:image" content="/images/og-image-campusnatura.jpg">
    <meta property="og:description" content="Formació per al naturalisme i la conservació, al camping Castell Mar, ubicat al Parc Natural dels Aiguamolls de l'Empordà.">
    <meta property="og:site_name" content="Campus Natura · Formació naturalista">
    <meta property="og:locale" content="ca">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    
    <!-- Twitter Card -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@CampusNatura">
    <meta name="twitter:creator" content="@CampusNatura">
    <meta name="twitter:url" content="https://campusnatura.org/">
    <meta name="twitter:title" content="Campus Natura · Formació naturalista">
    <meta name="twitter:description" content="Formació per al naturalisme i la conservació, al camping Castell Mar, ubicat al Parc Natural dels Aiguamolls de l'Empordà.">
    <meta name="twitter:image" content="/images/og-image-campusnatura.jpg">
    
    <?php wp_head(); ?>

</head>


<body <?php body_class(); ?>>

    <!-- Page Wrapper -->
    <div id="page-wrapper">
        
        <div class="language">
            <?php qtranxf_generateLanguageSelectCode('text') ?>
        </div>

        <!-- Banner -->
        <header id="banner">
            <div class="inner">
                <img src="<?php echo get_template_directory_uri(); ?>/images/campus-natura-logo.svg" alt="Campus Natura logo" width="400" height="280" />
                
                <h1><?php echo get_bloginfo( 'name' ); ?></h1>
                <h2><?php echo get_bloginfo( 'description' ); ?></h2>
                
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                <ul class="actions special">
                    <li><a href="#courses" class="button primary js-scroll">Informa't</a></li>
                </ul>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                <ul class="actions special">
                    <li><a href="#courses" class="button primary js-scroll">Infórmate</a></li>
                </ul>
                <?php endif; ?>
                <?php } ?>
            </div>
            <a href="#courses" class="more js-scroll-2"><span>Més informació</span></a>
        </header>
        
        
        <div class="svg-container"> 
            <span></span>

            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" standalone="no" encoding="utf-8" preserveAspectRatio="none" viewBox="0 0 1025 726"><path id="sunset" fill="#c3deeb" d="M-5.8-9.3h974.7v299H-5.8z"/><path id="cloud" fill="#FFFFFF" opacity="0.7" d="M274.7 117.6c12.9-2.9 23.5 6 23.5 18.7.5.3 1.3.6 1.9 1 5.7 3.3 8.5 9.4 7.1 15.4-1.4 6.1-6.6 10.4-13.1 10.9-.8.1-1.5 0-2.4 0-27.9 0-55.6 0-83.5-.1-2.9 0-5.9-.5-8.6-1.4-6.7-2.5-10.8-9.2-10.6-16.5.2-7.1 4.7-13.5 11.6-15.9 1-.4 2.1-.7 3.1-1-1.5-14.9 12-25 26.2-18.5 7.4-12.2 15.3-16.5 26.1-14.5 10.1 1.7 16.3 9 18.7 21.9z"/><path id="cloud2" fill="#FFFFFF" opacity="0.5" d="M393 122.3c2.7-15.3 10.9-24.5 23.3-26.8 13.1-2.4 23.3 3.2 31.7 17.1 18.9-5.2 32.1 6.6 34.1 21.6 2.2-.3 4.3-.9 6.5-.9 8.5-.1 15.5 6.5 15.7 14.9.3 8.7-6 15.8-14.6 16.3-1.1.1-2.3.1-3.3.1h-91.2c-3.5 0-7-.3-10.2-1.6-8.9-3.9-13.8-12.9-12.5-22.9 1.3-9 8.9-16.3 18.1-17.5.7-.2 1.5-.2 2.4-.3z"/><g id="cloud3" fill="#FFFFFF"><path opacity="0.6" d="M691.6 135.5c8-.6 12.7 2.5 15.9 10.7.5 0 1.1.1 1.7.1 4.7.4 8.3 4.2 8.5 8.9.2 4.4-3.1 8.5-7.5 9.3-1 .2-2.1.3-3.1.3h-69.4c-1 0-1.9 0-2.9-.2-3.8-.7-6.3-3.9-6.3-7.7s2.7-6.9 6.5-7.5c.8-.1 1.6-.2 2.6-.3.3-3.8 1.6-7.2 4.8-9.6 3.2-2.4 6.8-2.9 10.9-2.1 3.5-8.1 9.4-13.2 18.5-13.7 9-.4 15.4 3.9 19.8 11.8z"/><path opacity="0.8" d="M699.1 134.6c1.6-9.3 6.6-15.7 15.6-17.9 9-2.2 16.3 1.2 21.9 8.5 3.3-.6 6.6-.3 9.8 1 6.8 2.8 11.1 10 10.5 17.1-.8 8.3-7.2 14.3-14 15.3-1.6.3-3.3.3-5.1.3-14.9 0-29.8.1-44.8 0-6.1 0-10.9-3.8-12.4-9.4-1.3-4.9-.1-9.2 3.5-12.7 3.7-3.4 8-4.2 12.9-2.8.4.1.7.3 1 .4.4-.1.6 0 1.1.2z"/></g><path id="cloud4" fill="#FFFFFF" d="M178.4 163.2c1.2-21.9 23.5-38.4 48-23.8 11.8-17.3 28.2-25.3 49-21.3 20.5 4 32.5 17.6 37.1 38.1 17.9.5 32.7 7.2 42.6 22.6 7.2 11.2 9.3 23.5 6.6 36.5-4.7 22.7-30.1 47.1-67.3 33.8-.9 1.4-1.9 2.8-2.9 4.2-10.9 14.9-30.6 19.1-46.5 9.5-3.3-2-5.7-2.1-9.1-.9-22.7 8.2-47.4-2.5-57-24.8-1.6-3.7-3.6-5.2-7.5-6-15.6-3.4-26.5-17.6-26.3-33.7s11.6-29.9 27.3-33c1.8-.3 3.8-.6 6-1.2z"/><path id="sea" d="M968.5 288.7c.5 270.5.5 45.2 0 313.7-9 3.2-88.3-11.9-190.8-38.8-172.3-40.7-213.3-71.8-321-107.7-29.7-9.9-58.4-26.1-85.4-42.2-9.2-5.5-18-11.7-25.1-19.7-13.3-14.9-12-28.3 3.8-40.7 7-5.4 14.8-9.4 22.9-12.8 25.9-10.9 53.2-17.2 80.7-22.4 75.3-14.3 151.5-20.8 227.9-25 66.5-3.8 168.5-4.4 244.4-4.4 18.5-.3 12.5 0 42.6 0z" fill="#a4c7e4"/><path id="river" fill="#A1BEE0" d="M-5.4 475.5V467c23.2-1.3 26.4.5 47.6-.7 63.8-1.3 129.8-3.5 131.4-3.7 8-.3 46.9-1.5 58.3-3 47.2-3.6 94.5-7.3 141.7-10.9 4.9-.4 9.9-.5 13.8-4.2 20.7 10.8 41.3 21.6 62 32.4-40-.3-79.9-.6-120-.6-100.5-.3-201-.3-301.6-.5-8.1-.2-14.6.3-19.8.3-4.6-.2-8.7-.4-13.4-.6z"/><path id="green3" fill="#9DB593" d="M570.9 196.9c-26.4 4.4-32.5 11.4-59.4 20.8-24.1 8.5-45.7 6.9-66.8-7-5.7-3.8-11.4-7.7-16.6-12.2-18.8-12.5-29.2-27.4-45.2-39.4 32 4.2 44.3-.1 75.7-6.1 28.8-5.4 61.3 7.4 74.6 15.4 15.8 3.7 39.8 28.1 37.7 28.5z"/><g id="green2"><path class="st10" fill="#B0CC9B" d="M968.8 688h-974l-.5-211s121.9 4.1 129.2 4.2c34.2.5 134.5 4.6 170.6 13.9 43.4 11.2 117.8 60 172.9 71.2 118.3 24.1 204.9 29.6 345.4 34.5 100.2 3.5 156.2 6 156.2 6V688h.2zM104.8 134c23.8 1.4 92.9 25.9 135.1 30 37.9 3.7 54.1-32.6 91.9-28.2 19.4 2.2 35.9 2.3 57.9 7.2 1.8.3 2.6.7 3.9 1.4 14.7 10.9 27.8 24 41.4 36.1 4.7 4.2 9.9 7.8 15.2 11.2 19.3 12.8 39.7 15.4 60.8 5.1 17.5-8.5 35.7-13.7 55.2-14 2.2-.6 5.9-1.3 7.5-.8 3.7 1.2 3.9 1.8 5.9 2.5 36.6 12.2 69.9 31.4 102.8 51.2 10.3 6.2 21.3 11.2 33.2 14 .8.4 1.7.6 2.5 1 1.6 2.2 3.2 4.4 4.9 6.5 4.9 5.9 10.2 11.5 14.6 17.7 5.4 7.8 2.9 11.7-6.5 13.4-4.5.8-9.3.5-13.9.2-116.7-6.3-233.4-6.2-349.8 6.1l-118 24.2c-17.1 6.1-33.5 13.7-46.5 26.9s-16.4 28.7-10.2 46.1c3.3 9.4 8.4 17.8 14.5 25.7 10.2 13.4 22.3 24.8 35 35.8 1.7 1.4 4.3 2.3 4.5 5.7-2.1.5-4.2 1.1-6.2 1.6C229 462-8.1 469.7-5.1 467.7c-1.4-165.4-1.5-184.8-1.5-184.8S53.7 131 104.8 134z"/></g><path id="green1" fill="#BBC495" d="M579.1 282.2c-8.6-3.3-24.1-21.2-33.2-22.6-1.1-.2-83-18.7-109.4-19.9-80.1 0-159.8 34.5-172.4 42.4-5.6 3.5-16.5 30.5-21.9 34.2 51.3-31.3 289.5-34.5 336.9-34.1z"/><g id="beach"><path class="st12" fill="#F2D999" d="M449.5 476.8c25.7 3 48.6 14.5 72.6 22.7 66.6 22.8 130.8 53.5 199.3 69.6 68.1 16 131.7 25.2 247.5 37.2-219.2 3.3-365.6-2-543.9-49.9-36.3-15.6-91.9-38.7-121.8-57-6-3.7-12.5-3.9-18.9-4.5-182.4-16.8-158.5-6.7-188.8-10.1-28.8.2-80.1-2.4-100.9-3.6 0-3.1 0-4 .1-6.6 100.6.1 234.3 1.5 334.8 1.7 40-.2 80 .2 120 .5zM742.1 290.6c-50.2-1.5-100.4.3-150.5 3.5-56.3 3.7-112.6 9.3-167.9 21-25 5.2-49.6 11.6-72.9 22.4-8.2 3.8-16.1 8.3-22.9 14.5-13.9 12.6-15.3 26.3-4.1 41.5 5.8 7.8 13.1 14 20.8 19.7 10.6 7.9 22 14.4 33.5 20.7 4.3 2.4 9 4.6 9.2 10.6-3.9 3.7-9 3.8-13.8 4.2-47.2 3.6-83.2 7.3-130.5 10.9 0-.9-7.8-8.6-9.5-10-12.7-10.9-24.9-22.4-35-35.8-6-7.9-11.1-16.3-14.5-25.7-6.1-17.5-2.6-32.9 10.2-46.1 13-13.3 29.4-20.8 46.5-26.9 38.2-13.7 77.9-20.1 118-24.2 116.5-12.2 233.1-10.3 349.8-4.1 4.6.3 9.4.6 13.9-.2 9.4-1.6 11.9-7.6 6.5-15.4-4.3-6.2-9.8-11.8-14.6-17.7-1.7-2.1-3.3-4.3-4.9-6.5 20.4 4.8 41.2 6.1 62 4.5 52.3-3.9 100 11.8 146.4 33.5 1.8.8 4.9 1.4 4.2 4.2-.7 3.2-4 2.3-6.2 2.3l-157-.9c-5.2-.4-11-.2-16.7 0z"/></g></svg>

        </div>