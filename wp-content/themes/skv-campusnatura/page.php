<?php get_header(); ?>


<section id="courses" class="wrapper special">
    <div class="inner">

        <header class="major">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <?php the_content(); ?>

        <?php endwhile; ?>

        <?php else: ?>

                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

        <?php endif; ?>

    </div>
</section>



<?php get_sidebar(); ?>

<?php get_footer(); ?>
